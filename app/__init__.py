from flask import Flask
from .api.v1.models import db
from .api.v1.views import jwt
from .api.v1.views import cors

def create_app(config_name):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object('config.default')
    app.config.from_object(config_name)
    app.config.from_pyfile('config.py')
    
    cors.init_app(app)
    jwt.init_app(app)
    db.init_app(app)
    from .api.v1.models import Subscriber, Blog, Reservations, Meals, Orders, MealOrders
    with app.app_context():
        db.create_all()
    
    from .api.v1 import version1 as v1
    app.register_blueprint(v1, url_prefix='/api/v1')

    return app