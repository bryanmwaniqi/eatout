from app.api.v1.models import SubscriberSchema
import json

def test_new_user(client):
    """
    GIVEN the SubscriberSchema using
    WHEN a new User/Subscriber is created
    THEN check the username, email, password, confirm_password,and role fields are defined correctly
    """
    data = {
        'username':'denney', 
        'email':'denney@gmail.com', 
        'password':'ironman23', 
        'confirm_password':'ironman23'
        }
    user_schema = SubscriberSchema()
    user = user_schema.load(data)
    new_user_schema = SubscriberSchema(only=('username', 'email', 'password', 'confirm_password'))
    output = new_user_schema.dump(user)
    assert output 
    # assert output.username == "denney"
    # assert output.email == 'denney@gmail.com'
    # assert output.password == 'ironman23'
    # assert output.confirm_password == 'ironman23'