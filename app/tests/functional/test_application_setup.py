import pytest
from flask import current_app

def test_app_client(client):
    assert client
    
def test_app_configurations(client):
    current_app.testing == True
    assert current_app.config["TESTING"] == True
    assert current_app.config["JWT_COOKIE_SECURE"] == False
    assert current_app.config["JWT_COOKIE_CSRF_PROTECT"] == False

# def test_app(app):
#     assert app