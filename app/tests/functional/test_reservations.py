import json

def register(client, username, email, password, confirm_password):
    return client.post('/api/v1/register', 
    data=json.dumps({'username':username, 
    'email':email, 
    'password':password, 
    'confirm_password':confirm_password
    }), content_type='application/json')

def login(client, username, password):
    return client.post('/api/v1/login', data=json.dumps({'username':username, 'password':password}), content_type='application/json')

def logout(client):
    return client.get('/api/v1/logout')
 
def test_reservations(client):
    login(client, 'test', 'tester')
    resp = client.get('/api/v1/reservations')
    assert resp.status_code == 200, 'The response status code should be 200'
    assert json.loads(resp.data) == []