import json

def test_meals(client):
    resp = client.get('/api/v1/meals')
    assert resp.status_code == 200, 'The response status code should be 200'
    assert json.loads(resp.data) == []

def test_single_meal(client):
    response = client.get('/api/v1/meal/periperi')
    assert response.status_code == 404, 'The response status code should be 404'
    assert json.loads(response.data) ==  {'message': 'There is no meal as periperi'}
 

# def test_meal_addition(client, logged_in_client):
#     meal_data = [
# 	                {"name": "shawarma", 
#                     "price": 14.50, 
#                     "category": "pizza", 
#                     "description": "A very delicious pizza", 
#                     "image_url": "assets/pepperoni-pizza.jpg"
#                     }
#                 ]
#     resp = client.post('/api/v1/meals', data={meal_data})
#     assert resp.status_code == 201, 'The response status code should be 201'