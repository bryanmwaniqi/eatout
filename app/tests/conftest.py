import pytest

from .. import create_app
from ..api.v1.models import db
from app.api.v1.models import SubscriberSchema


@pytest.fixture(scope="session")
def client():
    """"flask application test client fixture"""
    app = create_app('config.testing')
    app_ctx = app.app_context()
    app_ctx.push()
    client = app.test_client()
    test_data = {'username':'test', 
    'email':'test@email.com', 
    'password':'tester', 
    'confirm_password':'tester'}
    test_schema = SubscriberSchema()
    test_user = test_schema.load(test_data)
    db.session.add(test_user)
    db.session.commit()
    yield client
    # Explicitly close DB connection
    db.session.close()
    db.drop_all()
    app_ctx.pop()

# @pytest.yield_fixture(scope='function')
# def db(client):
    # """A database for the tests."""
    # _db.app = app
    # _db = db
    # with app.app_context():
    #     _db.create_all()

    # yield _db

    # Explicitly close DB connection
    # _db.session.close()
    # _db.drop_all()

# @pytest.fixture(scope="session")
# def client(app):
#     client = app.test_client(use_cookies=True)
#     yield client

# @pytest.fixture
# def logged_in_client(client):
#     data = {'username':'test', 
#     'email':'test@email.com', 
#     'password':'tester', 
#     'confirm_password':'tester'}
#     register(client, data['username'], data['email'], data['password'], data['confirm_password'])
#     login(client, data['username'], data['password'])
#     yield client
#     logout(client)

# def register(client, username, email, password, confirm_password):
#     return client.post('/api/v1/register', data={'username':username, 'email':email, 'password':password, 'confirm_password':confirm_password})

# def login(client, username, password):
#     return client.post('/api/v1/login', data={'username':username, 'password':password})

# def logout(client):
#     return client.get('/api/v1/logout')
