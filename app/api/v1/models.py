from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from marshmallow import Schema, fields , validate, post_load, validates_schema, ValidationError
from flask_marshmallow import Marshmallow	
from flask_jwt_extended import current_user

db = SQLAlchemy()

# flask-sqlalchemy models
class Blog(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    post = db.Column(db.Text, nullable=False)
    date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    author_id = db.Column(db.Integer, db.ForeignKey('subscriber.id'), nullable=False)

    def __repr__(self):
        return "Blogpost, {}, posted on {}".format(self.title, self.date_posted)

class Subscriber(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100), nullable=False, unique=True)
    email = db.Column(db.String(30), nullable=False, unique=True)
    password = db.Column(db.String(50), nullable=False)
    confirm_password = db.Column(db.String(50), nullable=False)
    posts = db.relationship('Blog', backref='author')
    user_reservations = db.relationship('Reservations', backref='reserver')
    orders = db.relationship('Orders', backref='customer')

    def __repr__(self):
        return "User, {}, at {}".format(self.username, self.email)


class MealOrders(db.Model):
    meal_id = db.Column(db.Integer, db.ForeignKey('meals.id'), primary_key=True)
    order_id = db.Column(db.Integer, db.ForeignKey('orders.id'), primary_key=True)
    meal_name = db.Column(db.String)
    product_tally = db.Column(db.Integer, nullable=False)
    product_total = db.Column(db.Float, nullable=False)
    order = db.relationship('Orders', backref='ordered_meals')
    meal = db.relationship('Meals', backref='orders')

class Meals(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40), nullable=False)
    category = db.Column(db.String, nullable=False)
    description = db.Column(db.Text, nullable=False)
    image_url = db.Column(db.String, nullable=False)
    price = db.Column(db.Float, nullable=False)

class Orders(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    order_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    status = db.Column(db.String, default="shipping")
    customer_id = db.Column(db.Integer, db.ForeignKey('subscriber.id'), nullable=False)
    overall_total = db.Column(db.Float)
    # payment = db.relationship('Payment', backref='order')

# class Payment(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     order_id = db.Column(db.Integer, db.ForeignKey("Orders", uselist=False), nullable=False)
#     status = db.Column(db.String, default="settled")
#     amount = db.Column(db.Float, nullable=False)
#     provider = db.Column(db.String)
#     payment_type = db.Column(db.String, nullable=False)
#     payment_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

class Reservations(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    persons = db.Column(db.Integer, nullable=False)
    reservation_date = db.Column(db.Date, nullable=False)
    reserver_id = db.Column(db.Integer, db.ForeignKey('subscriber.id'), nullable=False)
    table_no = db.Column(db.Integer, nullable=False)
    phone = db.Column(db.String, nullable=False)

# marshmallow schemas

class BlogpostSchema(Schema):
    # fields to validate
    title = fields.Str(required=True)
    post = fields.Str(required=True)
    date_posted = fields.DateTime(required=True, dump_only=True)
    author_id = fields.Integer()

    class Meta:
        model = Blog

    @validates_schema
    def validate_blog_title(self, data, **kwargs):
        blog_post = Blog.query.filter_by(title=data['title']).first()
        if blog_post:
            raise ValidationError('Blog title already exists.')

    @post_load
    def make_blogpost(self, data, **kwargs):
        data["author_id"] = current_user.id
        post = Blog(**data)
        db.session.add(post)
        db.session.commit()
        return post

class SubscriberSchema(Schema):
    # fields to validate
    username = fields.Str(required=True)
    email = fields.Email(required=True)
    password = fields.Str(required=True, load_only=True)
    confirm_password = fields.Str(required=True)
    posts = fields.Nested('BlogpostSchema', exclude=('author_id',), many=True)
    user_reservations = fields.Nested(lambda: ReservationSchema, exclude=('reserver_id',), many=True)
    orders = fields.Nested(lambda: OrderSchema, exclude=('customer_id',), many=True)

    class Meta:
        model = Subscriber

    @validates_schema
    def validate_user(self, data, **kwargs):
        user = Subscriber.query.filter_by(username=data['username']).first()
        if user:
            raise ValidationError('username already taken.')
        email = Subscriber.query.filter_by(email=data['email']).first()
        if email:
            raise ValidationError('email already registered.')
        if data['password'] != data['confirm_password']:
            raise ValidationError('Your confirmation password needs to match your password.')

    @post_load
    def make_user(self, data, **kwargs):
        return Subscriber(**data)
    

class MealSchema(Schema):
    # fields to validate
    name = fields.Str(required=True)
    category = fields.Str(required=True)
    price = fields.Float(required=True)
    description = fields.Str(required=True)
    image_url = fields.Str(required=True)

    class Meta:
        model = Meals

    @validates_schema
    def validate_meal_name(self, data, **kwargs):
        meal = Meals.query.filter_by(name=data['name']).first()
        if meal:
            raise ValidationError('Meal already in product catalog.')

    @post_load
    def make_meal(self, data, **kwargs):
        meal = Meals(**data)
        db.session.add(meal)
        db.session.commit()
        return meal

class ReservationSchema(Schema):
    # fields to validate
    persons = fields.Int(required=True)
    reservation_date = fields.Date(required=True)
    reserver_id = fields.Int()
    phone = fields.String(required=True)
    table_no = fields.Int(required=True)

    class Meta:
        model = Reservations

    @validates_schema
    def validate_reservation(self, data, **kwargs):
        if data['table_no'] > 20 or data['table_no'] < 1:
            raise ValidationError('You can only reserve from Table 1 - 20')
        if data['persons'] > 6 or data['persons'] < 1:
            raise ValidationError('Table accomodates atleast 1 person and at most 6')
        reservation = Reservations.query.filter_by(table_no=data['table_no'], reservation_date=data['reservation_date']).first()
        if reservation:
            raise ValidationError('Table already reserved for that date.')

    @post_load
    def make_reservation(self, data, **kwargs):
        data["reserver_id"] = current_user.id
        reservation = Reservations(**data)
        db.session.add(reservation)
        db.session.commit()
        return reservation

class OrderSchema(Schema):
    # fields to validate
    order_date = fields.DateTime(dump_only=True)
    status = fields.String(dump_only=True)
    customer_id = fields.Int()
    overall_total = fields.Float(dump_only=True)
    ordered_meals = fields.Nested(lambda: MealOrdersSchema, exclude=('order_id',), many=True)

    class Meta:
        model = Orders

    @post_load
    def make_order(self, data, **kwargs):
        data["customer_id"] = current_user.id
        return Orders(**data)

class MealOrdersSchema(Schema):
    # fields to validate
    order_id = fields.Integer(dump_only=True)
    meal_id = fields.Integer(dump_only=True)
    product_tally = fields.Integer()
    product_total = fields.Float()
    meal_name = fields.String()

    class Meta:
        model = MealOrders

