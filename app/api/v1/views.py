from flask import jsonify, make_response, request, render_template
from flask_restful import Resource, reqparse, abort
from marshmallow import ValidationError, EXCLUDE
from .models import (db, Subscriber, Blog, Reservations, Meals, Orders, MealOrders, 
                    SubscriberSchema, MealSchema, BlogpostSchema, OrderSchema, ReservationSchema)
from flask_jwt_extended import (JWTManager, jwt_required, create_access_token, 
                                current_user, get_jwt, unset_jwt_cookies, set_access_cookies,get_jwt_identity)
from flask_cors import CORS

jwt = JWTManager()
cors = CORS(supports_credentials=True)
blocklist = set()

# This function will be called whenever create_access_token
# is used. It will take whatever object is passed into the
# create_access_token method, and define what the identity
# of the access token will be.
@jwt.user_identity_loader
def user_identity_lookup(user):
    return user.username

# This function is called whenever a protected endpoint is accessed and after
# the token is verified, and must return an object based on the tokens identity.
@jwt.user_lookup_loader
def curr_user_loader(_jwt_header, jwt_data):
    identity = jwt_data["sub"]
    return Subscriber.query.filter_by(username=identity).one_or_none()

@jwt.token_in_blocklist_loader
def check_if_token_in_blocklist(jwt_header, jwt_payload):
    jti = jwt_payload['jti']
    return jti in blocklist

# @jwt.token_in_blacklist_loader
# def check_if_token_in_blacklist(decrypted_token):
#     jti = decrypted_token['jti']
#     return jti in blacklist

class VerifyUser(Resource):
    @jwt_required(optional=True)
    def get(self):
        cur_user = get_jwt_identity()
        if cur_user:
            return jsonify({"status" : "logged-in", "username": cur_user})
        else:
            return jsonify({"status" : "logged-out", "username": "unknown"})

class UserRegistration(Resource):
    """User registration resource"""
    def post(self):
        """creates a new user"""
        data = request.get_json()
        user_schema = SubscriberSchema()
        error = user_schema.validate(data)
        if error:
            return jsonify(error)
        new_user = user_schema.load(data)
        db.session.add(new_user)
        db.session.commit()
        new_user_schema = SubscriberSchema(only=('username', 'email'))
        output = new_user_schema.dump(new_user)
        return output, 201


class UserLogin(Resource):
    """User Login resource"""
    def post(self):
        """logs in a user after authentication and authorization"""
        data = request.get_json()
        user = Subscriber.query.filter_by(username=data['username']).first()
        if not user or user.password != data['password']:
            abort(401, message='Wrong username and/or password combination')  
        access_token = create_access_token(identity=user)
        resp = jsonify({"status" : "logged-in", "username": user.username})
        set_access_cookies(resp, access_token)
        return resp

class Logout(Resource):
    """User logout functionality"""
    @jwt_required()
    def get(self):
        resp = jsonify({"status": "logged out"})
        resp.status_code = 200
        unset_jwt_cookies(resp)
        # jti = get_jwt()['jti']
        # blocklist.add(jti)
        return resp

class AllUsers(Resource):
    """Gets all the users data for a user who's a super admin"""
    @jwt_required()
    def get(self):
        all_users = Subscriber.query.all()
        users_schema = SubscriberSchema(only=('username', 'email'), many=True)
        output = users_schema.dump(all_users)
        return jsonify({"users" : output}) 

class MealProducts(Resource):
    """Meals resource"""
    def get(self):
        """gets all the meals"""
        all_meals = Meals.query.all()
        meal_schema = MealSchema(many=True)
        output = meal_schema.dump(all_meals)
        # resp = jsonify(output)
        # resp.headers['Access-Control-Allow-Origin'] = '*'
        return output, 200
    
    @jwt_required()
    def post(self):
        """creates a new meal product"""
        data = request.get_json()
        meal_schema = MealSchema(many=True)
        error = meal_schema.validate(data)
        if error:
            abort(400, message=error)
        try:
            new_meals = meal_schema.load(data)
        except ValidationError as err:
            abort(400, message=err.messages)
        output = meal_schema.dump(new_meals)
        return output, 201

class SingleMeal(Resource):
    """Single Meal resource"""
    def get(self, meal_name):
        """gets a single meal"""
        meal = Meals.query.filter_by(name=meal_name).first_or_404(description='There is no meal as {}'.format(meal_name))
        meal_schema = MealSchema()
        output = meal_schema.dump(meal)
        return output

class TableReservations(Resource):
    """User reservations resource"""
    @jwt_required()
    def get(self):
        """gets all the reservations belonging to a single user"""
        # reservations = Reservations.query.all()
        # reserve_schema = ReservationSchema(many=True)
        # output = reserve_schema.dump(reservations)
        # return output, 200
        reserve_schema = ReservationSchema(many=True)
        output = reserve_schema.dump(current_user.user_reservations)
        return jsonify(output)

    @jwt_required()
    def post(self):
        """adds a reservations"""
        data = request.get_json()
        reserve_schema = ReservationSchema(many=True)
        error = reserve_schema.validate(data)
        if error:
            return jsonify(error)
        try:
            new_reservations = reserve_schema.load(data)
        except ValidationError as err:
            return jsonify(err)
        output = reserve_schema.dump(new_reservations)
        return output, 201

class BlogPosts(Resource):
    """Admin Blogposts"""
    def get(self):
        """gets all the blogposts"""
        all_posts = Blog.query.all()
        posts_schema = BlogpostSchema(many=True)
        output = posts_schema.dump(all_posts)
        return output, 200

    @jwt_required()
    def post(self):
        """adds a blog post by admin only"""
        data = request.get_json()
        blog_schema = BlogpostSchema(many=True)
        error = blog_schema.validate(data)
        if error:
            abort(400, message=error)
        try:
            new_posts = blog_schema.load(data)
        except ValidationError as err:
            abort(400, message=err.messages)
        output = blog_schema.dump(new_posts)
        return output, 201

class CustOrders(Resource):
    """Customer Meal orders"""
    @jwt_required()
    def get(self):
        """gets all orders by a certain user_id"""
        orders_schema = OrderSchema(many=True)
        output = orders_schema.dump(current_user.orders)
        return output, 200

    @jwt_required()
    def post(self):
        """places a new customer order"""
        data = request.get_json()
        order_schema = OrderSchema()
        for meal in data["meals"]:
            if len(meal) > 2 or len(meal) < 2 or "name" not in meal.keys() or "product_tally" not in meal.keys(): 
                abort(400, message="please supply the meal name and product tally")
            if type(meal["name"]) != str:
                abort(400, message="The meal name should be a string")
            if type(meal["product_tally"]) != int or meal["product_tally"] < 1 or meal["product_tally"] > 15:
                abort(400, message="Product tally should be a positive integer between 1 and 15")
            meal_prod = Meals.query.filter_by(name=meal["name"]).first()
            if not meal_prod:
                abort(404, message="no such meal as {}".format(meal["name"]))
        new_order = order_schema.load(data, unknown=EXCLUDE)
        total = 0
        for meal in data["meals"]:
            meal_prd = Meals.query.filter_by(name=meal["name"]).first()
            ot = meal["product_tally"] * meal_prd.price
            meal_order = MealOrders(product_tally=meal["product_tally"], product_total= ot, meal_name=meal_prd.name)
            meal_order.meal = meal_prd
            meal_order.order = new_order
            total += ot
            db.session.add(meal_order)
            db.session.commit()
        new_order.overall_total = total
        db.session.add(new_order)
        db.session.commit()
        output = order_schema.dump(new_order)
        return output, 201
