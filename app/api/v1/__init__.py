from flask import Blueprint
from flask_restful import Api
from .views import (UserRegistration, AllUsers, MealProducts, SingleMeal, UserLogin, TableReservations,
                    BlogPosts, CustOrders, Logout, VerifyUser)

version1 = Blueprint('version1', __name__)

api = Api(version1)

api.add_resource(UserRegistration, '/register')
api.add_resource(AllUsers, '/users')
api.add_resource(Logout, '/logout')
api.add_resource(VerifyUser, '/verify')
api.add_resource(MealProducts, '/meals')
api.add_resource(SingleMeal, '/meal/<string:meal_name>')
api.add_resource(UserLogin, '/login')
api.add_resource(TableReservations, '/reservations')
api.add_resource(BlogPosts, '/blog')
api.add_resource(CustOrders, '/orders')