# Eatout

[![Build Status](https://app.travis-ci.com/bryanmwaniqi/Eatout.svg?branch=ch-testing)](https://app.travis-ci.com/bryanmwaniqi/Eatout)
[![Coverage Status](https://coveralls.io/repos/bitbucket/bryanmwaniqi/eatout/badge.svg?branch=master)](https://coveralls.io/bitbucket/bryanmwaniqi/eatout?branch=master)

**Version alpha**

A pizza ordering web app flask API consumable from any frontend since its CORS enabled.

## contributors

- Brian Mwaniki <bryanmwaniqi@gmail.com>

