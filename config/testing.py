import os

SQLALCHEMY_DATABASE_URI = 'postgresql://postgres@127.0.0.1:5432/travis_ci_eatout'
TESTING=True
JWT_COOKIE_SECURE = False
JWT_COOKIE_CSRF_PROTECT = False
